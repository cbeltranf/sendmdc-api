<?php

namespace Inmovsoftware\SendmdcApi\Providers;

use Illuminate\Support\ServiceProvider;

use Inmovsoftware\SendmdcApi\Http\Resources\V1\GlobalCollection;

class InmovTechSendmdcServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\SendmdcApi\Models\V1\Sendmdc');
        $this->app->make('Inmovsoftware\SendmdcApi\Http\Controllers\V1\SendmdcController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
